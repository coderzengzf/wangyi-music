# vue-wangyiyun

> A Vue.js project

## Node

感谢网易云接口提供的后端接口

[接口文档](https://binaryify.github.io/NeteaseCloudMusicApi/#/)

git clone https://github.com/Binaryify/NeteaseCloudMusicApi

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
