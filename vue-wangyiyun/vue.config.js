module.exports = {
  dev: {
    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},

    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 8080, // 可改默认8080
    autoOpenBrowser: true,//每次启动是否自动开浏览器默认不
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-
    proxyTable:{//代理模式
      '/api':{//碰到/api的就当作暗号接头走转换
          target:'http://127.0.0.1:3000',  // 真实地址
          changeOrigin: true ,// 是否跨域
          pathRewrite:{
            '^/api':''  //实际不存在这个前缀暗号''去掉，有的话就不写
        }
      }
    }
  }
}
