import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const FindPage = () => import('../views/FindPage/findpage.vue');
const TheVideo = () => import('../views/the-video/theVideo.vue');
const User = () => import('../views/user/user.vue');
const Login = () => import('../views/login/login.vue');
const LoginUser = () => import('../views/login/login-user.vue');

const routes = [
  {
    path: '/',
    redirect:'/login'
  },
  {
    path:'/login',
    component: Login
  },
  {
    path:'/login-user',
    component:LoginUser
  },
  {
    path: '/findpage',
    component: FindPage
  },
  {
    path:'/the-video',
    component: TheVideo
  },
  {
    path:'/user',
    component: User
  },
  {
    path: '/friends',
    component: () => import('../views/firends/firends.vue')
  },
  {
    path: '/songlist/:id',
    name: 'songlist',
    component: () => import('../views/songlist/songlist.vue')
  },
  {
    path: '/player/:id',
    name: 'player',
    component: () => import('../views/player/player.vue')
  },
  {
    path:'/singer/:id',
    component: () => import('../views/SingerList/singer-list.vue')
  },
  {
    path:'/album/:id',
    component: () => import('../views/Album/Album.vue')
  },
  {
    path: '/singSheet',
    component: () => import('../views/user/singSheet.vue')
  },
  {
    path: '/search',
    component: () => import('../views/Search/search.vue')
  },
  {
    path: '/radio',
    component: () => import('../views/radio/radio.vue')
  },
  {
    path: '/playmv/:id',
    component: () => import ('../components/play-mv/play-mv.vue')
  },
  {
    path: '/playvideo/:id',
    component: () => import ('../views/play-video/play-video.vue')
  }
]

const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}

const router = new Router({
  routes,
  mode:'history'
})


// router.beforeEach((to, from, next) => {
//   if (to.path === '/login') {
//     next();
//   } else {
//     let token = sessionStorage.getItem("token");
 
//     if (token === null || token === '') {
//       Notify('您还未登录，请先登录');
//       next('/login');
//     } else {
//       next();
//     }
//   }
  
// })

export default router