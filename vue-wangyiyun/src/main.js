// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueLazyLoad from 'vue-lazyload'

import Vant from 'vant'
Vue.use(Vant);
import 'vant/lib/index.css';

Vue.config.productionTip = false

import '@/assets/js/iconfont.js'
import '@/assets/js/iconfont1.js'
import '@/assets/js/iconfont2.js'
import '@/assets/css/static.css'

Vue.use(VueLazyLoad,{
  loading:require('./common/image/default.jpg')
})


// 全局注册cookies
import cookies from 'vue-cookies'
Vue.prototype.$cookies = cookies

// 全局注册session
import session from './common/js/session'
Vue.prototype.$session = session

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
