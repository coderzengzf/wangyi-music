import { request } from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getBanner(){
  return request({
    url: `/dj/banner`,
    method: 'get'
  })
}

export function getDjCatelist(){
  return request({
    url: `/dj/catelist`,
    method: 'get'
  })
}

export function getRecommendDj(){
  return request({
    url: '/program/recommend',
    method: 'get'
  })
}

export function getradioToplist(){
  return request({
    url: `/dj/program/toplist`,
    method: 'get'
  })
}

export function getCategoryList(type){
  return request({
    url: `/dj/recommend/type?type=${type}`,
    method: 'get'
  })
}

export function getHotRadio(cateId){
  return request({
    url: `/dj/radio/hot?cateId=${cateId}`,
    method: 'get',
  })
}