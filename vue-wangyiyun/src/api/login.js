import { request } from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getLoginInfo(phone, password) {
  return request({
    url: `/login/cellphone?phone=${phone}&password=${password}&&cookie=${cookie}`,
    method: 'get',
    // headers: {"content-type": "application/json"},
    // data: data
  })
}

export function getUserInfo (id){
  return request({
    url: `/user/detail?uid=${id}`,
    method: 'get',
    // headers: {"content-type": "application/json"},
  })
  // const url = HOST + `/user/detail?uid=${id}`

  // return axios.get(url)
}

export function getUserMusicList(id){
  return request({
    url: `user/playlist?uid=${id}`,
    method: 'get',
  })

}

export function getAccountInfo(){
  return request({
    url: `user/playlist?uid=${id}`,
    method: 'get',
  })
}

export function getUserDetail(){
  return request({
    url: `/user/subcount`,
    method: 'get',
  })
}

export function getLoginStatus(){
  return request({
    url: `/login/status`,
    method: 'get',
  })
}

export function getRefresh(){
  return request({
    url: `/login/refresh?cookie=${cookie}`,
    method: 'get',
  })
}

export function getLoginOut(){
  return request({
    url: `/logout`,
    method: 'get',
  })
}
