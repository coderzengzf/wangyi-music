import { request} from './request'
let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getCreateList(name){
  return request({
    url: `/playlist/create?name=${name}&&cookie=${cookie}`,
    method: 'get',
  })
}

export function getDelList(id){
  return request({
    url: `/playlist/delete?id=${id}&&cookie=${cookie}`,
    method: 'get',
  })
  // const url = HOST + `/playlist/delete?id=${id}`

  // return axios.get(url);
}