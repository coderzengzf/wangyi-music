import { request } from './request'

export function getSearchTxt(){
  return request({
    url: `/search/default`,
    method: 'get',
  })
}

export function getSearchHot(){
  return request({
    url: `/search/hot`,
    method: 'get',
  })
}

export function getSearchDetail(){
  return request({
    url: `/search/hot/detail`,
    method: 'get'
  })
}

export function getSearchSuggest(params){
  return request({
    url: '/search/suggest',
    method: 'get',
    params
  })
}

export function getSearchInfo(params){
  return request({
    url: `/cloudsearch`,
    method: 'get',
    params
  })
}

// export function getSearchSinger (name) {
//   const url = HOST + `/search?keywords=${name}&type=100`

//   return axios.get(url)
// }

// export function getSearchSongs (name, page) {
//   const url = HOST + `/search?keywords=${name}&offset=${page}`

//   return axios.get(url)
// }

// export function getSearchSuggest (name) {
//   const url = HOST + `/search/suggest?keywords=${name}`

//   return axios.get(url)
// }

// export function getSongDetail (id) {
//   const url = HOST + `/song/detail?ids=${id}`

//   return axios.get(url)
// }

// export function getSearchHot (id) {
//   const url = HOST + `/search/hot`

//   return axios.get(url)
// }
