import {request} from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

// 个性推荐
export function getHomeDragon(){
  return request({
    url: `/homepage/dragon/ball`,
    method: 'get',
  })
}

export function getBanner () {
  return request({
    url: `/banner`,
    method: 'get',
  })
}

export function getRecommendList () {
  return request({
    url: `/personalized`,
    method: 'get',
  })
}

export function getNewSong () {
  return request({
    url: `/personalized/newsong`,
    method: 'get',
  })
}

// 歌单数据
export function getPlayList(){
  return request({
    url: `/playlist/hot?cookie=${cookie}`,
    method: 'get',
  })
}

export function getPlayListDetail(id){
  return request({
    url: `/playlist/track/all?id=${id}&&cookie=${cookie}`,
    method:'get'
  })
}

export function getRelevantList(id){
  return request({
    url: `/related/playlist?id=${id}`,
    method: 'get'
  })
}

// 排行榜数据
export function getRankList(){
  return request({
    url: `/toplist?cookie=${cookie}`,
    method: 'get'
  })
}

export function getRankDetail(id){
  return request({
    url: `/playlist/detail?id=${id}&cookie=${cookie}`,
    method: 'get'
  })
}

// 歌手数据
export function getSingerList(type, area, initial,limit){
  return request({
    url: `/artist/list?type=${type}&area=${area}&initial=${initial}&limit=${limit}&cookie=${cookie}`,
    // url: `/artist/list?type=${type}&area=${area}&initial=${initial}&limit=${limit}&cookie=${cookie}`,
    method: 'get'
  })
}