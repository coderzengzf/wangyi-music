import { request } from './request'


let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getAllMV(area){
  return request({
    url: `/mv/all?area=${area}`,
    method: 'get',
  })
}

export function getNewMv(){
  return request({
    url: `/mv/first?cookie=${cookie}`,
    method: 'get',
  })
}

export function getWangyiMv(){
  return request({
    url: '/mv/exclusive/rcmd?limit=10',
    method: 'get',
  })
}

export function getRecommendMv(){
  return request({
    url: `/personalized/mv?cookie=${cookie}`,
    method: 'get'
  })
}

export function getMvRank(area){
  if(area === '全部'){
    return request({
      url: `/top/mv?limit=10&&cookie=${cookie}`,
      method:'get',
    })
  }else{
    return request({
      url: `/top/mv?limit=10&&area=${area}&&cookie=${cookie}`,
      method:'get',
    })
  }
}


export function getMvDetail(id){
  return request({
    url: `/mv/url?id=${id}`,
    method: 'get'
  })
}

export function getMvInfo(id){
  return request({
    url: `/mv/detail/info?mvid=${id}`,
    method: 'get'
  })
}


export function getMvDetailInfo(id){
  return request({
    url: `/mv/detail?mvid=${id}`,
    method: 'get'
  })
}

export function getSimiMv(id){
  return request({
    url: `/simi/mv?mvid=${id}`,
    method: 'get'
  })
}

export function getMvComment(params){
  return request({
    url: '/comment/mv',
    method: 'get',
    params
  })
}