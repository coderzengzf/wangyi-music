import { request } from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getHotSong(id){
  return request({
    url: `/artists/top/song?id=${id}`,
    method: 'get'
  })
}

export function getAllSongs(params){
  return request({
    url: '/artist/songs',
    method: 'get',
    params
  })
}

export function getSingerAlbum(id){
  return request({
    url: `/artist/album?id=${id}`,
    method: 'get'
  })
}

export function getSingerMv(id){
  return request({
    url: `/artist/mv?id=${id}`,
    method: 'get'
  })
}

export function getSinegrDesc(id){
  return request({
    url: `/artist/desc?id=${id}`,
    method: 'get'
  })
}

export function getSimilarSinger(id){
  return request({
    url: `/simi/artist?id=${id}`,
    method: 'get'
  })
}

export function getAlbumDetail(id){
  return request({
    url: `/album?id=${id}`,
    method: 'get'
  })
}