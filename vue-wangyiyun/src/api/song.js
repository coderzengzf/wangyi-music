import { request } from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getDetailSong (id) {
  return request({
    url: `/playlist/detail?id=${id}&cookie=${cookie}`,
    method: 'get'
  })
}

export function getSongCheck(id){
  return request({
    url: `/check/music?id=${id}`,
    method:'get'
  })
}

export function getPlayerList(id){
  return request({
    url: `/lyric?id=${id}`,
    method:'get'
  })
}

export function getSongDetail(id){
  return request({
    url: `/song/detail?ids=${id}`,
    method: 'post',
  })
}

export function getSongUrl(id){
  return request({
    url: `/song/url?id=${id}`,
    method: 'get'
  })
}
