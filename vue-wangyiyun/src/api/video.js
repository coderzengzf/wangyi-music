// import axios from 'axios'
import { request } from './request'

let cookieStr = window.sessionStorage.getItem('cookie');
let cookie = encodeURIComponent(cookieStr);

export function getVideoTag(){
  return request({
    // url: `/video/group/list?cookie=${cookie}`,
    url: '/video/group/list',
    method: 'get',
  })
}

export function getVideoList(id){
  return request({
    // url: `/video/group?id=${id}&cookie=${cookie}`,
    url: `/video/group?id=${id}&cookie=${cookie}`,
    method: 'get',
  })
}

export function getVideoCategoryList(){
  return request({
    // url: `/video/category/list?cookie=${cookie}`,
    url: `/video/category/list?cookie=${cookie}`,
    method: 'get',
  })
}

export function getAllVideo(){
  return request({
    // url: `/video/timeline/all?cookie=${cookie}`,
    url: `/video/timeline/all?cookie=${cookie}`,
    method: 'get',
  })
}

export function  getRecommendVideo(offset) {
  return request({
    url: `/video/timeline/recommend?offset=${offset}&cookie=${cookie}`,
    // url: `/video/timeline/recommend?offset=${offset}`,
    method: 'get',
  })
}


export function getVideoUrl(params){
  return request({
    url: `/video/url`,
    method:'get',
    params
  })
}

export function getVideoDetail(params){
  return request({
    url: `/video/detail`,
    method: 'get',
    params
  })
}

export function getVideoInfo(params){
  return request({
    url: `/video/detail/info`,
    method: 'get',
    params
  })
}

export function getRelatedVideo(params){
  return request({
    url: '/related/allvideo',
    method: 'get',
    params
  })
}

export function getVideoComment(params){
  return request({
    url: '/comment/video',
    method: 'get',
    params
  })
}